/*
*  Separate chaining hashtable
*/
#ifndef __CHAINING_HASH_H
#define __CHAINING_HASH_H
// Standard library includes
#include <iostream>
#include <vector>
#include <list>
#include <stdexcept>
#include <cmath>
 
// Custom project includes
#include "Hash.h"
 
// Namespaces to include
using std::vector;
using std::list;
using std::pair;
using std::cout;
using std::endl;
 
//
// Separate chaining based hash table - derived from Hash
//
template<typename K, typename V>
class ChainingHash : public Hash<K, V> {
private:
    int tableSize = 101; //Default tableSize is 101
 
    int numElements = 0; //Start numElements is 0

    vector< list< pair< K, V > > > table; //Initialize actual hash table

public:
    ChainingHash(int n = 101)
    {
        tableSize = n; //Set tableSize
        
        table.resize(tableSize); //Resize hash table to size
    }
    ~ChainingHash()
    {
        this->clear(); //Delete all elements of table
    }
    bool empty()
    {
        for (int i = 0; i < tableSize; i++) //Loops through each list in table
        {
            if (!table[i].empty()) //If any list is not empty
            {
                return 0; //Signify non-empty table
            }
        }
        return 1; //No elements in any list in hash table, table is empty
    }
    int size()
    {
        return numElements; //Number of elements in the table
    }
    V& at(const K& key)
    {
        int hashKey = hash(key); //Calculates table key from hash function
        
        typename list<pair<K, V> >::iterator it = table[hashKey].begin(); //Iterator to find value from list at hash key in table
        
        while (it != table[hashKey].end()) //Loop until end of list
        {
            if (it->first == key) //If element matching given key is found
            {
                cout << "Item \"" << it->second << "\" found!\n" << std::endl; //Notify user

                return it->second; //Return value associated with key
            }
 
            it++; //Increment iterator
        }
        cout << "No item found with key " << key << std::endl;
 
        throw std::out_of_range("Key not in hash"); //Key was not found
    }
    V& operator[](const K& key)
    {
        int hashKey = hash(key); //Calculate table key from hash function
        
        typename list<pair<K, V> >::iterator it = table[hashKey].begin(); //Iterator to find value from list at hash key in table
        
        while (it != table[hashKey].end()) //Loop until end of list
        {
            if (it->first == key) //If element matching given key is found
            {
                cout << "Item \"" << it->second << "\" found!\n" << std::endl;

                return it->second; //Return value associated with key
            }
 
            it++; //Increment iterator
        }
        cout << "No item found with key " << key << std::endl; //Notify user
 
        throw std::out_of_range("Key not in hash"); //Key was not found
    }
    int count(const int& key)
    {
        int hashKey = hash(key); //Calculate table key from hash function
        
        typename list<pair<K, V> >::iterator it = table[hashKey].begin(); //Iterator to find value from list at hash key in table
        
        while ((it != table[hashKey].end()) && (it->first != key)) //Loop until end of list
        {
            it++; //Increment iterator
        }
        
        if (it->first == key) //Key was found
        {
            return 1; //One element with key
        }

        return 0; //No element with key exists
    }
    void emplace(K key, V value)
    {
        pair<K, V> newPair = { key, value }; //Initialize new pair to insert
        
        insert(newPair); //Insert newly made pair
    }
    void insert(const std::pair<K, V>& nPair)
    {
        int hashKey = hash(nPair.first); //Calculate table key from hash function
        
        table[hashKey].push_back(nPair); //Set pair in bucket associated with key
 
        numElements++; //Increment number o elements
        
        if (load_factor() > 0.75) //Check load factor
        {
            rehash(); //Rehash if load factor is larger than 0.75
        }
    }
    void erase(const K& key)
    {
        int hashKey = hash(key); //Calculate table key from hash function
        
        typename list<pair<K, V> >::iterator it = table[hashKey].begin(); //Iterator to find value from list at hash key in table
        
        while ((it->first != key) && (it != table[hashKey].end())) //Loop until correct key is found
        {
            it++; //Increment iterator
        }
        if (it->first == key) //If correct key was found
        {
            table[hashKey].erase(it); //Delete pair with key

            numElements--; //Update number of elements in table
        }
        else
        {
            cout << "No element with key " << key << "found." << std::endl; //No element with searched key found
        }
    }
    void clear()
    {
        numElements = 0;
 
        for (int i = 0; i < tableSize; i++) //Loop through table
        {
            if (!table[i].empty()) //It list is not empty
            {
                table[i].clear(); //Clear list
            }
        }
    }
    int bucket_count()
    {
        return tableSize;
    }
    int bucket_size(int n)
    {
        return table[n].size(); //Return length of list at bucket number n
    }
    int bucket(const K& key)
    {
        int hashKey = hash(key); //Calculate table key from hash function
        
        typename list<pair<K, V> >::iterator it = table[hashKey].begin(); //Iterator to find value from list at hash key in table
        
        while ((it->first != key) && (it != table[hashKey].end())) //Loop until key is found
        {
            it++; //Increment iterator
        }
        if (it->first == key) //If key was found
        {
            return hashKey; //Return bucket number of element
        }
        else
        {
            throw std::out_of_range("Key not in hash"); //Key was not found
        }
    }
    float load_factor()
    {
        return size() / (float)tableSize; //Calculate load factor
    }
    void rehash()
    {
        cout << "Rehashing..." << std::endl;
 
        int newSize = findNextPrime(tableSize * 2); //Set new size to next prime larger than twice previous size
        
        rehash(newSize); //Rehash table
    }
    void rehash(int n)
    {
        vector< list< pair< K, V > > > oldTable(table); //Create table to hold elements of old table
 
        int oldSize = tableSize;
 
        clear(); //Clear old table
 
        table.resize(n); //Resize old table
 
        tableSize = n; //Set new table size
        
        for (int i = 0; i < oldSize; i++) //Loop through elements
        {
            typename list< pair< K, V> >::iterator it = oldTable[i].begin(); //Iterator to find value from list at hash key in table
            
            while (it != oldTable[i].end()) //Loop through each bucket of old table
            {
                insert(*it); //Reinsert pair into table
                
                it++; //Increment iterator
            }
        }
    }
private:
    int findNextPrime(int n)
    {
        while (!isPrime(n))
        {
            n++;
        }
        return n;
    }
    int isPrime(int n)
    {
        for (int i = 2; i <= sqrt(n); i++)
        {
            if (n % i == 0)
            {
                return false;
            }
        }
        return true;
    }
    int hash(const int& key)
    {
        return key % tableSize;
    }
};
#endif //__CHAINING_HASH_H
