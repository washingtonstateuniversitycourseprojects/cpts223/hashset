prog: main.o
	g++ main.o -o runProg -fopenmp

main.o: main.cpp ChainingHash.h Hash.h ProbingHash.h
	g++ -c -g -Wall -fopenmp -std=c++11 main.cpp

clean:
	-rm *.o

run: @./runProg