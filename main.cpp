/************************************************************************
* Programmer: Emma Mickas                                               *
* CptS 223, Spring 2020                                                 *
* Programming Assignment 3                                              *
* February 29, 2020                                                     *
* Decription: This file contains the main function for the program.     *
* Questions:                                                            *
* Serial Implementations:   The Chaining hash was slower than the       *
*                           Probing hash except for when finding a value*
*                           not in the table. I expected this because   *
*                           working with pairs should take less time    *
*                           than working with lists of pairs, but in the*
*                           case of a value not in the table, the       *
*                           probing table had to increment more than the*
*                           chaining table.                             *
* Parallel vs. Serial Implementation:                                   *
*                           The parallel implementations of the hash    *
                            took the same amount of time as the serial  *
*                           implementations when one core was used, and *
*                           a bit less time with multiple cores. This   *
*                           makes sense because using one core is the   *
*                           same as not setting a particular number of  *
*                           threads, and setting more than one core     *
*                           means distributing tasks among threads that *
*                           can perform at the same time, reducing total*
*                           time.                                       *
* What could I change:                                                  *
*                           I could try to separate the different       *
*                           threads by task or by data myself instead of*
*                           allowing the processor to manage which      *
*                           threads handle tasks/data. I could try to   *
*                           separate based on the amount of time taken  *
*                           for a task, one thread in charge of         *
*                           rehashing while the others insert into a    *
*                           placeholder with new size until rehashing   *
*                           is completed so that all threads still work *
*                           during rehashing.                           *
************************************************************************/


#include "ChainingHash.h"
#include "ProbingHash.h"
#include "ParallelProbingHash.h"
#include <ctime>
#include <fstream>
 
#define NUM_THREADS_1 1
#define NUM_THREADS_CORE 4
int main()
{
    std::ofstream outfile; //Initiallize outfile

    outfile.open("HashAnalysis.txt"); //Open outfile for writing

    ChainingHash<int, int> chainingTable(101); //Initiallize chaining hash table

    ProbingHash<int, int> probingTable(101); //Initiallize probing hash table

    clock_t start; //Initialize start variable for tracking time taken to do tasks
 
    clock_t end; //Initialize end variable for tracking time taken to do tasks
 
    int value; //Initialize value for tracking at and [] functions
 
    start = clock(); //Start timer
    
    for (int i = 1; i <= 1000000; i++) //Insert values 1 - 1000000
    {
        if (i % 10000 == 0) //Track insertion at multiples of 10000
        {
            cout << "Inserting element " << i << " into chaining hash.\n" << std::endl;
        }
 
        chainingTable.emplace(i, i); //Insert element
    }
    
    end = clock(); //End timer
    
    outfile << "Elapsed time in seconds for insert on chaining hash: " << ((double)(end - start))/CLOCKS_PER_SEC << "\n" << std::endl; //Write time to outfile
 
    cout << "Elapsed time in seconds for insert on chaining hash: " << ((double)(end - start))/CLOCKS_PER_SEC << "\n" << std::endl; //Write time to user

    start = clock(); //Start timer
    
    for (int i = 1; i <= 1000000; i++) //Insert values 1 - 1000000
    {
        if (i % 10000 == 0) //Track insertion at multiples of 10000
        {
            cout << "Inserting element " << i << " into probing hash.\n" << std::endl;
        }
 
        probingTable.emplace(i, i); //Insert element
    }
    
    end = clock(); //End timer
    
    outfile << "Elapsed time in seconds for insert on probing hash: " << ((double)(end - start)) / CLOCKS_PER_SEC << "\n" << std::endl; //Write time to outfile
 
    cout << "Elapsed time in seconds for insert on probing hash: " << ((double)(end - start)) / CLOCKS_PER_SEC << "\n" << std::endl; //Write time to user

    start = clock(); //Start timer

    try //Attempt to find variable
    {
        value = chainingTable.at(177); //Test finding value
    }
    catch(const std::exception& e) //Catch out of range error
    {
        cout << "Key not found.\n" << std::endl; //Notify user of out of range
    }
 
    end = clock(); //End timer
 
    outfile << "Elapsed time in seconds for find \"177\" on chaining hash: " << ((double)(end - start))/CLOCKS_PER_SEC << "\n" << std::endl; //Write time to outfile
 
    cout << "Elapsed time in seconds for find \"177\" on chaining hash: " << ((double)(end - start))/CLOCKS_PER_SEC << "\n" << std::endl; //Write time to user

    start = clock(); //Start timer
 
    try //Attempt to find variable
    {
        value = probingTable.at(177); //Test finding value
    }
    catch(const std::exception& e) //Catch out of range error
    {
        cout << "Key not found.\n" << std::endl; //Notify user of out of range
    }
 
    end = clock(); //End timer
 
    outfile << "Elapsed time in seconds for find \"177\" on probing hash: " << ((double)(end - start)) / CLOCKS_PER_SEC << "\n" << std::endl; //Write time to outfile

    cout << "Elapsed time in seconds for find \"177\" on probing hash: " << ((double)(end - start)) / CLOCKS_PER_SEC << "\n" << std::endl; //Write time to user

    start = clock(); //Start timer
 
    try //Attempt to find variable
    {
        value = chainingTable.at(2000000); //Test finding value
    }
    catch(const std::exception& e) //Catch out of range error
    {
        cout << "Key \"2000000\" not found.\n" << std::endl; //Notify user of out of range
    }
 
    end = clock(); //End timer
 
    outfile << "Elapsed time in seconds for find \"2000000\" on chaining hash: " << ((double)(end - start))/CLOCKS_PER_SEC << "\n" << std::endl; //Write time to outfile

    cout << "Elapsed time in seconds for find \"2000000\" on chaining hash: " << ((double)(end - start))/CLOCKS_PER_SEC << "\n" << std::endl; //Write time to user

    start = clock(); //Start timer
 
    try //Attempt to find variable
    {
        value = probingTable.at(2000000); //Test finding value
    }
    catch(const std::exception& e) //Catch out of range error
    {
        cout << "Key \"2000000\" not found.\n" << std::endl; //Notify user of out of range
    }
 
    end = clock(); //End timer
 
    outfile << "Elapsed time in seconds for find \"2000000\" on probing hash: " << ((double)(end - start)) / CLOCKS_PER_SEC << "\n" << std::endl; //Write time to outfile

    cout << "Elapsed time in seconds for find \"2000000\" on probing hash: " << ((double)(end - start)) / CLOCKS_PER_SEC << "\n" << std::endl; //Write time to user

    start = clock(); //Start timer
 
    chainingTable.erase(177); //Test erase
 
    end = clock(); //End timer
 
    outfile << "Elapsed time in seconds to erase \"177\" from chaining hash: " << ((double)(end - start))/CLOCKS_PER_SEC << "\n" << std::endl; //Write time to outfile

    cout << "Elapsed time in seconds to erase \"177\" from chaining hash: " << ((double)(end - start))/CLOCKS_PER_SEC << "\n" << std::endl; //Write time to user

    start = clock(); //Start timer
 
    probingTable.erase(177); //Test erase
 
    end = clock(); //End timer
 
    outfile << "Elapsed time in seconds to erase \"177\" from probing hash: " << ((double)(end - start)) / CLOCKS_PER_SEC << "\n" << std::endl; //Write time to outfile

    cout << "Elapsed time in seconds to erase \"177\" from probing hash: " << ((double)(end - start)) / CLOCKS_PER_SEC << "\n\n" << std::endl; //Write time to user

    outfile << "\nFinal chaining table size: " << chainingTable.size() << "\n" << std::endl; //Write final size (number of elements) of chaining table to outfile
 
    outfile << "Final chaining table bucket count: " << chainingTable.bucket_count() << "\n" << std::endl; //Write final number of buckets of chaining table to outfile
 
    outfile << "Final chaining table load factor: " << chainingTable.load_factor() << "\n" << std::endl; //Write final load factor of chaining table to outfile

    outfile << std::endl;

    cout << "Final chaining table size: " << chainingTable.size() << "\n" << std::endl; //Write final size (number of elements) of chaining table to user
 
    cout << "Final chaining table bucket count: " << chainingTable.bucket_count() << "\n" << std::endl; //Write final number of buckets of chaining table to user
 
    cout << "Final chaining table load factor: " << chainingTable.load_factor() << "\n" << std::endl; //Write final load factor of chaining table to user
 
    cout << std::endl;

    outfile << "Final probing table size: " << probingTable.size() << "\n" << std::endl; //Write final size (number of elements) of probing table to outfile
 
    outfile << "Final probing table bucket count: " << probingTable.bucket_count() << "\n" << std::endl; //Write final number of buckets of probing table to outfile
 
    outfile << "Final probing table load factor: " << probingTable.load_factor() << "\n" << std::endl; //Write final load factor of probing table to outfile
 
    outfile << std::endl;

    cout << "Final probing table size: " << probingTable.size() << std::endl; //Write final size (number of elements) of probing table to user
 
    cout << "Final probing table bucket count: " << probingTable.bucket_count() << "\n" << std::endl; //Write final number of buckets of probing table to user
 
    cout << "Final probing table load factor: " << probingTable.load_factor() << "\n" << std::endl; //Write final load factor of probing table to user
 
    cout << std::endl;

    outfile << "\n\n\n";

    ParallelProbingHash<int, int> parallelTable(101);
 
    omp_set_num_threads(NUM_THREADS_1);
 
    start = clock(); //Start timer

    #pragma omp parallel for schedule(static)
    for (int i = 1; i <= 1000000; i++) //Insert values 1 - 1000000
    {
        if (i % 100000 == 0) //Track insertion at multiples of 10000
        {
            cout << "Inserting element " << i << " into parallel hash with one core.\n" << std::endl;
        }

        parallelTable.emplace(i, i); //Insert element

        if (parallelTable.load_factor() > 0.75) //Check load factor
        {
            parallelTable.rehash(); //Rehash if load factor is larger than 0.75
        }
    }

    end = clock(); //End timer

    outfile << "Elapsed time in seconds for insert on probing hash with one core: " << ((double)(end - start))/CLOCKS_PER_SEC << "\n" << std::endl; //Write time to outfile

    cout << "Elapsed time in seconds for insert on probing hash with one core: " << ((double)(end - start))/CLOCKS_PER_SEC << "\n" << std::endl; //Write time to user

    start = clock(); //Start timer

    try //Attempt to find variable
    {
        value = parallelTable.at(177); //Test finding value
    }
    catch(const std::exception& e) //Catch out of range error
    {
        cout << "Key not found.\n" << std::endl; //Notify user of out of range
    }

    end = clock(); //End timer

    outfile << "Elapsed time in seconds for find \"177\" on probing hash with one core: " << ((double)(end - start))/CLOCKS_PER_SEC << "\n" << std::endl; //Write time to outfile

    cout << "Elapsed time in seconds for find \"177\" on probing hash with one core: " << ((double)(end - start))/CLOCKS_PER_SEC << "\n" << std::endl; //Write time to user

    start = clock(); //Start timer

    try //Attempt to find variable
    {
        value = parallelTable.at(2000000); //Test finding value
    }
    catch(const std::exception& e) //Catch out of range error
    {
        cout << "Key \"2000000\" not found.\n" << std::endl; //Notify user of out of range
    }

    end = clock(); //End timer

    outfile << "Elapsed time in seconds for find \"2000000\" on parallel hash with one core: " << ((double)(end - start))/CLOCKS_PER_SEC << "\n" << std::endl; //Write time to outfile

    cout << "Elapsed time in seconds for find \"2000000\" on parallel hash with one core: " << ((double)(end - start))/CLOCKS_PER_SEC << "\n" << std::endl; //Write time to user

    start = clock(); //Start timer

    parallelTable.erase(177); //Test erase

    end = clock(); //End timer

    outfile << "Elapsed time in seconds to erase \"177\" from probing hash with one core: " << ((double)(end - start))/CLOCKS_PER_SEC << "\n" << std::endl; //Write time to outfile

    cout << "Elapsed time in seconds to erase \"177\" from probing hash with one core: " << ((double)(end - start))/CLOCKS_PER_SEC << "\n" << std::endl; //Write time to user

    outfile << "\nFinal parallel table size with one core: " << parallelTable.size() << "\n" << std::endl; //Write final size (number of elements) of parallel table with one core to outfile

    outfile << "Final parallel table bucket count with one core: " << parallelTable.bucket_count() << "\n" << std::endl; //Write final number of buckets of parallel table with one core to outfile

    outfile << "Final parallel table load factor with one core: " << parallelTable.load_factor() << "\n\n" << std::endl; //Write final load factor of parallel table with one core to outfile
 
    cout << "Final parallel table size with one core: " << parallelTable.size() << "\n" << std::endl; //Write final size (number of elements) of parallel table with one core to user

    cout << "Final parallel table bucket count with one core: " << parallelTable.bucket_count() << "\n" << std::endl; //Write final number of buckets of parallel table with one core to user

    cout << "Final parallel table load factor with one core: " << parallelTable.load_factor() << "\n\n" << std::endl; //Write final load factor of parallel table with one core to user
 
    parallelTable.clear();
 
    omp_set_num_threads(NUM_THREADS_CORE);

    start = clock(); //Start timer

    #pragma omp parallel for schedule(static)
    for (int i = 1; i <= 1000000; i++) //Insert values 1 - 1000000
    {
        if (i % 10000 == 0) //Track insertion at multiples of 10000
        {
            cout << "Inserting element " << i << " into parallel hash with four cores.\n" << std::endl;
        }

        parallelTable.emplace(i, i); //Insert element

        if (parallelTable.load_factor() > 0.75) //Check load factor
        {
            parallelTable.rehash(); //Rehash if load factor is larger than 0.75
        }
    }

    end = clock(); //End timer

    outfile << "\nElapsed time in seconds for insert on chaining hash with four cores: " << ((double)(end - start))/CLOCKS_PER_SEC << "\n" << std::endl; //Write time to outfile

    cout << "Elapsed time in seconds for insert on chaining hash with four cores: " << ((double)(end - start))/CLOCKS_PER_SEC << "\n" << std::endl; //Write time to user

    start = clock(); //Start timer

    try //Attempt to find variable
    {
        value = parallelTable.at(177); //Test finding value
    }
    catch(const std::exception& e) //Catch out of range error
    {
        cout << "Key not found.\n" << std::endl; //Notify user of out of range
    }

    end = clock(); //End timer

    outfile << "Elapsed time in seconds for find \"177\" on parallel hash with four cores: " << ((double)(end - start))/CLOCKS_PER_SEC << "\n" << std::endl; //Write time to outfile

    cout << "Elapsed time in seconds for find \"177\" on parallel hash with four cores: " << ((double)(end - start))/CLOCKS_PER_SEC << "\n" << std::endl; //Write time to user

    start = clock(); //Start timer

    parallelTable.erase(177); //Test erase

    end = clock(); //End timer

    outfile << "Elapsed time in seconds to erase \"177\" from parallel hash with four cores: " << ((double)(end - start))/CLOCKS_PER_SEC << "\n" << std::endl; //Write time to outfile

    cout << "Elapsed time in seconds to erase \"177\" from parallel hash with four cores: " << ((double)(end - start))/CLOCKS_PER_SEC << "\n" << std::endl; //Write time to user

    start = clock(); //Start timer

    try //Attempt to find variable
    {
        value = parallelTable.at(2000000); //Test finding value
    }
    catch(const std::exception& e) //Catch out of range error
    {
        cout << "Key \"2000000\" not found.\n" << std::endl; //Notify user of out of range
    }

    end = clock(); //End timer

    outfile << "Elapsed time in seconds for find \"2000000\" on parallel hash with four cores: " << ((double)(end - start))/CLOCKS_PER_SEC << "\n" << std::endl; //Write time to outfile

    cout << "Elapsed time in seconds for find \"2000000\" on parallel hash with four cores: " << ((double)(end - start))/CLOCKS_PER_SEC << "\n\n" << std::endl; //Write time to user

    outfile << "\nFinal parallel table size with four cores: " << parallelTable.size() << "\n" << std::endl; //Write final size (number of elements) of parallel table with four cores to outfile

    outfile << "Final parallel table bucket count with four cores: " << parallelTable.bucket_count() << "\n" << std::endl; //Write final number of buckets of parallel table with four cores to outfile

    outfile << "Final parallel table load factor with four cores: " << parallelTable.load_factor() << "\n\n" << std::endl; //Write final load factor of parallel table with four cores to outfile

    cout << "\nFinal parallel table size with four cores: " << parallelTable.size() << "\n" << std::endl; //Write final size (number of elements) of parallel table with four cores to user

    cout << "Final parallel table bucket count with four cores: " << parallelTable.bucket_count() << "\n" << std::endl; //Write final number of buckets of parallel table with four cores to user

    cout << "Final parallel table load factor with four cores: " << parallelTable.load_factor() << "\n\n" << std::endl; //Write final load factor of parallel table with four cores to user

    outfile << "\n\n\n";
}
