#ifndef __PARALLEL_PROBING_HASH_H
#define __PARALLEL_PROBING_HASH_H
 
#include <vector>
#include <stdexcept>
#include <omp.h>
#include <cmath>

#include "Hash.h"
 
using std::vector;
using std::pair;
 
// Can be used for tracking lazy deletion for each element in your table
enum EntryState2 {
    EMPTY2 = 0,
    VALID2 = 1,
    DELETED2 = 2
};
 
template<typename K, typename V>
class ParallelProbingHash : public Hash<K, V> { // derived from Hash
private:
 
    int tableSize = 101; //Default tableSize is 101
 
    int numElements = 0; //Start numElements is 0
    
    vector< pair< K, V > > table; //Initialize actual hash table
 
public:
    ParallelProbingHash(int n = 101)
    {
        tableSize = n; //Set tableSize
        
        table.resize(tableSize); //Resize hash table to size
    }
    ~ParallelProbingHash()
    {
        table.clear();
    }
    bool empty()
    {
        for (int i = 0; i < tableSize; i++) //Loop through vector
        {
            if (table[i].first != 0) //Check for empty pair
            {
                return 0; //Signify non-empty table
            }
        }
        return 1; //No elements in any list in hash table, table is empty
    }
    int size()
    {
        return numElements; //Number of elements in the table
    }
    V& at(const K& key)
    {
        int hashKey = hash(key); //Calculate table key from hash function
        
        while (table[hashKey].first != key) //Loop until correct key is found
        {
            hashKey++; //Probe to find correct key
 
            if (hashKey == hash(key)) //If full loop has been made
            {
                break; //Exit loop
            }
 
            if (hashKey == tableSize) //If end of table is reached
            {
                hashKey = 0; //Set hash key to start of table
            }
        }
 
        if (table[hashKey].first == key) //If key is found
        {
            cout << "Item \"" << table[hashKey].second << "\" found!\n" << std::endl;

            return table[hashKey].second; //Return value associated with key
        }
 
        throw std::out_of_range("Key not in hash"); //Key was not found
    }
    V& operator[](const K& key)
    {
        int hashKey = hash(key); //Calculate table key from hash function
        
        while (table[hashKey].first != key) //Loop until key is found
        {
            hashKey++; //Probe to find correct key
 
            if (hashKey == hash(key)) //If full loop has been made
            {
                break; //Exit loop
            }
 
            if (hashKey == tableSize) //If end of table is reached
            {
                hashKey = 0; //Set hash key to start of table
            }
        }
 
        if (table[hashKey].first == key) //If key is found
        {
            cout << "Item \"" << table[hashKey].second << "\" found!\n" << std::endl; //Notify user

            return table[hashKey].second; //Return value associated with key
        }
 
        throw std::out_of_range("Key not in hash"); //Key was not found
    }
    int count(const K& key)
    {
        int hashKey = hash(key);
        
        for (int i = hashKey; i < tableSize; i++) //Loops through entire hash table
        {
            if (hash(table[i].first) == key) //Loop to find all keys with same hashKey
            {
                return 1; //Increment counter
            }

            if (i == tableSize - 1) //End of table reached
            {
                i = -1; //Set i so that next iteration i will be zero
            }
            else if (i == hashKey - 1) //Full loop hash been made
            {
                return 0;  //Key was not found
            }
        }
    }
    void emplace(K key, V value)
    {
        #pragma omp critical
        {
            pair<int, int> newPair = { key, value }; //Initialize new pair to insert
                
            insert(newPair); //Insert newly made pair
        }
    }
    void insert(const std::pair<K, V>& pair)
    {
        int hashKey = hash(pair.first); //Calculate table key from hash function

        while (table[hashKey].first != 0) //If element already exists at bucket
        {
            hashKey++; //Probe until empty spot is found

            if (hashKey == tableSize) //If end of table is reached
            {
                hashKey = 0; //Set hash key to start of table
            }
        }
        
        table[hashKey] = pair; //Set pair in bucket associated with key

        numElements++; //Increment number o elements
    }
    void erase(const K& key)
    {
        #pragma omp critical
        {
            int hashKey = hash(key); //Calculate table key from hash function
            
            while (table[hashKey].first != key) //Loop until correct key is found
            {
                hashKey++; //Probe for correct key
    
                if (hashKey != hash(key)) //If full loop has been made
                {
                    break; //Exit loop
                }
            }
            
            if (table[hashKey].first == key) //If correct key was found
            {
                table[hashKey] = { 0, 0 }; //Reset pair with key

                numElements--; //Update number of elements in table
            }
            else
            {
                cout << "No element with key " << key << "found." << std::endl; //No element with searched key found
            }
        }
    }
    void clear()
    {
        numElements = 0;

        for (int i = 0; i < tableSize; i++) //Loop through table
        {
            table[i] = { 0, 0 }; //Reset each pair in table
        }
    }
    int bucket_count()
    {   
        return tableSize;
    }
    int bucket_size(int n)
    {
        if (table[n].first == 0) //If bucket is empty
        {
            return 0;
        }
        else //Bucket is not empty
        {
            return 1;
        }
    }
    int bucket(const K& key)
    {
        int hashKey = hash(key); //Calculate table key from hash function
        
        while (table[hashKey].first != key) //Loop until key is found
        {
            hashKey++; //Increment hashKey
 
            if (hashKey == hash(key)) //If entire table has been searched
            {
                break; //Break out of loop
            }
 
            if (hashKey == tableSize) //If end of table is reached
            {
                hashKey = 0; //Set hash key to start of table
            }
        }
        if (table[hashKey].first == key) //If key was found
        {
            return hashKey; //Return bucket number of element
        }
        else
        {
            throw std::out_of_range("Key not in hash"); //Key was not found
        }
    }
    float load_factor()
    {
        return size() / (float)tableSize; //Calculate load factor
    }
    void rehash()
    {
        int newSize = findNextPrime(tableSize * 2); //Set new size to next prime larger than twice previous size
        
        rehash(newSize); //Rehash table
    }
    void rehash(int n)
    {
        #pragma omp critical
        {
            vector<pair<K, V> > oldTable(table); //Create table to hold elements of old table

            int oldSize = tableSize;

            clear(); //Empty old table for new element placement
            
            table.resize(n); //Resize old table
            
            tableSize = n; //Set new hashtable size
            
            for (int i = 0; i < oldSize; i++) //Loop through each element of old table
            {
                if (oldTable[i].first != 0)
                {
                    insert(oldTable[i]); //Insert new pair into new table
                }
            }
        }
    }
private:
    int findNextPrime(int n)
    {
        while (!isPrime(n))
        {
            n++;
        }
        return n;
    }
    int isPrime(int n)
    {
        for (int i = 2; i <= sqrt(n); i++)
        {
            if (n % i == 0)
            {
                return false;
            }
        }
        return true;
    }
    int hash(const int& key)
    {
        return key % tableSize;
    }
};
 
#endif //__PROBING_HASH_H
