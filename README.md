# HashSet

This projects uses hash sets with different collision techniques to store integer data. It also repeats methods of insertion and data finding on the hash sets with different numbers of threads being used to test parallel vs serial implementation.